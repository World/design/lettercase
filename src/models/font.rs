use serde::{Deserialize, Serialize};
use std::any::Any;

pub trait FontCSSClass {
    fn get_class(&self) -> String;
}

#[derive(Debug, Clone, PartialEq, Eq, Ord, PartialOrd, Serialize, Deserialize)]
pub struct FontVariant {
    pub style: FontStyle,
    pub weight: FontWeight,
}
impl ToString for FontVariant {
    fn to_string(&self) -> String {
        if self.style != FontStyle::Normal {
            return format!("{} {}", self.weight.to_string(), self.style.to_string());
        }
        self.weight.to_string()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Ord, PartialOrd, Serialize, Deserialize)]
pub enum FontStyle {
    Italic,
    Oblique,
    Normal,
}

impl FontCSSClass for FontStyle {
    fn get_class(&self) -> String {
        match *self {
            FontStyle::Italic => "style-italic",
            FontStyle::Oblique => "style-oblique",
            FontStyle::Normal => "style-normal",
        }
        .to_string()
    }
}

impl ToString for FontStyle {
    fn to_string(&self) -> String {
        match *self {
            FontStyle::Italic => "Italic",
            FontStyle::Oblique => "Oblique",
            FontStyle::Normal => "Regular",
        }
        .to_string()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Ord, PartialOrd, Serialize, Deserialize)]
pub enum FontWeight {
    Thin = 100,
    ExtraLight = 200,
    Light = 300,
    Normal = 400, // regular
    Medium = 500,
    SemiBold = 600,
    Bold = 700,
    ExtraBold = 800,
    Heavy = 900,
}

impl FontCSSClass for FontWeight {
    fn get_class(&self) -> String {
        match *self {
            FontWeight::Thin => "weight-thin",
            FontWeight::ExtraLight => "weight-ultra-light",
            FontWeight::Light => "weight-light",
            FontWeight::Medium => "weight-medium",
            FontWeight::SemiBold => "weight-semi-bold",
            FontWeight::Bold => "weight-bold",
            FontWeight::ExtraBold => "weight-extra-bold",
            FontWeight::Heavy => "weight-heavy",
            _ => "weight-regular",
        }
        .to_string()
    }
}

impl ToString for FontWeight {
    fn to_string(&self) -> String {
        match *self {
            FontWeight::Thin => "Thin",
            FontWeight::ExtraLight => "Ultra-Light",
            FontWeight::Light => "Light",
            FontWeight::Medium => "Medium",
            FontWeight::SemiBold => "Semi-Bold",
            FontWeight::Bold => "Bold",
            FontWeight::ExtraBold => "Ultra-Bold",
            FontWeight::Heavy => "Heavy",
            _ => "Regular",
        }
        .to_string()
    }
}

pub enum FontStretch {
    UltraCondensed,
    ExtraCondensed,
    Condensed,
    SemiCondensed,
    Normal,
    SemiExpanded,
    Expanded,
    ExtraExpanded,
    UltraExpanded,
}

pub trait FontViewable {
    fn is_installed(&self) -> bool;

    fn get_family(&self) -> String;

    fn get_variants(&self) -> Vec<FontVariant>;

    fn get_normal_variants(&self) -> Vec<FontVariant> {
        let variants = self.get_variants();
        let mut normal_variants = variants.into_iter().filter(|variant| variant.style == FontStyle::Normal).collect::<Vec<FontVariant>>();
        normal_variants.dedup();
        normal_variants
    }

    fn get_all_weights(&self) -> Vec<FontWeight> {
        let variants = self.get_variants();
        let mut weight_variants = variants.iter().map(|variant| variant.weight).collect::<Vec<FontWeight>>();
        weight_variants.dedup();
        weight_variants
    }

    fn get_all_styles(&self) -> Vec<FontStyle> {
        let variants = self.get_variants();
        let mut style_variants = variants.iter().map(|variant| variant.style).collect::<Vec<FontStyle>>();
        style_variants.dedup();
        style_variants
    }

    fn as_any(&self) -> &dyn Any;
}
