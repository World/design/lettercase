// ObjectWrapper is a GObject subclass, which we need to carry the rustio::Station/song::Song struct.
// With this we can use gtk::ListBox bind_model() properly.
//
// For more details, you should look at this gtk-rs example:
// https://github.com/gtk-rs/examples/blob/master/src/bin/listbox_model.rs
// Source https://gitlab.gnome.org/World/Shortwave/blob/master/src/model/object_wrapper.rs

use gtk::prelude::*;
use serde::de::DeserializeOwned;

use glib::subclass::prelude::*;

use crate::api::{GoogleFont, SystemFont};
use crate::models::FontViewable;

// TODO Replace this wrapper with a proper object instead of having a common
// trait for system/Google fonts, this serializes and deserializes the same
// object over and over.
mod imp {
    use super::*;
    use std::cell::RefCell;

    pub struct ObjectWrapper {
        data: RefCell<Option<String>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ObjectWrapper {
        const NAME: &'static str = "ObjectWrapper";
        type ParentType = glib::Object;
        type Type = super::ObjectWrapper;

        fn new() -> Self {
            Self { data: RefCell::new(None) }
        }
    }

    impl ObjectImpl for ObjectWrapper {
        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: once_cell::sync::Lazy<Vec<glib::ParamSpec>> = once_cell::sync::Lazy::new(|| {
                vec![glib::ParamSpecString::new(
                    "data",
                    "Data",
                    "Data",
                    None, // Default value
                    glib::ParamFlags::READWRITE,
                )]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _obj: &Self::Type, _id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
            match pspec.name() {
                "data" => self.data.replace(value.get().unwrap()),
                _ => unimplemented!(),
            };
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "data" => self.data.borrow().to_value(),
                _ => unimplemented!(),
            }
        }
    }
}

glib::wrapper! {
    pub struct ObjectWrapper(ObjectSubclass<imp::ObjectWrapper>);
}

impl ObjectWrapper {
    pub fn new<O>(object: O) -> ObjectWrapper
    where
        O: serde::ser::Serialize,
    {
        glib::Object::new(&[("data", &serde_json::to_string(&object).unwrap())]).unwrap()
    }

    pub fn deserialize<O>(&self) -> O
    where
        O: DeserializeOwned,
    {
        let data = self.property::<String>("data");
        serde_json::from_str(&data).unwrap()
    }

    pub fn family_name(&self) -> String {
        let data = self.property::<String>("data");
        if let Ok(font) = serde_json::from_str::<SystemFont>(&data) {
            font.get_family()
        } else {
            let font = serde_json::from_str::<GoogleFont>(&data).unwrap();
            font.get_family()
        }
    }
}
