use crate::api::google_fonts;
use crate::api::system;
use crate::api::{GoogleFont, SystemFont};
use crate::application::Action;
use crate::models::{FontViewable, ObjectWrapper};

use async_std::channel;
use async_std::sync::Arc;
use gio::prelude::*;
use glib::Sender;

#[derive(Clone, PartialEq)]
pub enum SortOrder {
    Asc,
    Desc,
}

#[derive(Clone, PartialEq)]
pub enum SortBy {
    Family,
    // LastModified,
}

impl From<&str> for SortBy {
    fn from(sortby: &str) -> Self {
        match sortby {
            "family" => SortBy::Family,
            //"last_modified" => SortBy::LastModified,
            _ => SortBy::Family,
        }
    }
}

#[derive(Debug)]
pub struct FontsModel {
    pub explore_model: Arc<gio::ListStore>,
    pub installed_model: Arc<gio::ListStore>,
    sender: Sender<Action>,
}

impl FontsModel {
    pub fn new(sender: Sender<Action>) -> Arc<Self> {
        let explore_model = Arc::new(gio::ListStore::new(ObjectWrapper::static_type()));
        let installed_model = Arc::new(gio::ListStore::new(ObjectWrapper::static_type()));

        let model = Arc::new(Self {
            explore_model,
            installed_model,
            sender,
        });

        model.init(model.clone());
        model
    }

    pub fn init(&self, fm: Arc<Self>) {
        // Download the fonts
        let sender = self.sender.clone();
        spawn!(async move {
            fm.fetch_fonts().await.and_then(move |()| {
                send!(sender, Action::ViewLibrary);
                Ok(())
            });
        });
    }

    async fn fetch_fonts(&self) -> Result<(), Box<dyn std::error::Error + std::marker::Send + std::marker::Sync>> {
        if let Ok(fonts) = google_fonts::fetch_fonts().await {
            for font in fonts {
                self.add_explore_font(&font);
            }
        }
        let (tx, rx) = channel::bounded(1);
        system::fetch_fonts(tx);
        let fonts: Vec<SystemFont> = rx.recv().await.unwrap();
        for font in fonts {
            self.add_system_font(&font);
        }
        Ok(())
    }

    fn get_sytem_font_id(&self, font: &SystemFont) -> Option<u32> {
        for i in 0..self.installed_model.as_ref().n_items() {
            if let Some(installed_font) = self.installed_model.item(i) {
                let installed_font: SystemFont = installed_font.downcast_ref::<ObjectWrapper>().unwrap().deserialize();
                if font.get_family() == installed_font.get_family() {
                    return Some(i);
                }
            }
        }
        None
    }

    pub async fn refresh_system_fonts(&self) {
        let (tx, rx) = channel::bounded(1);
        system::fetch_fonts(tx);
        let fonts: Vec<SystemFont> = rx.recv().await.unwrap();
        for font in fonts {
            if self.get_sytem_font_id(&font).is_none() {
                self.add_system_font(&font);
            }
        }
    }

    pub fn add_explore_font(&self, font: &GoogleFont) {
        let object = ObjectWrapper::new(font);

        let fonts_compare = move |a: &glib::Object, b: &glib::Object| -> std::cmp::Ordering {
            let font_a: GoogleFont = a.downcast_ref::<ObjectWrapper>().unwrap().deserialize();
            let font_b: GoogleFont = b.downcast_ref::<ObjectWrapper>().unwrap().deserialize();

            font_b.get_normal_variants().len().cmp(&font_a.get_normal_variants().len())
        };
        self.explore_model.insert_sorted(&object, move |a, b| fonts_compare(a, b));
    }

    pub fn add_system_font(&self, font: &SystemFont) {
        let object = ObjectWrapper::new(font);

        let fonts_compare = move |a: &glib::Object, b: &glib::Object| -> std::cmp::Ordering {
            let font_a: SystemFont = a.downcast_ref::<ObjectWrapper>().unwrap().deserialize();
            let font_b: SystemFont = b.downcast_ref::<ObjectWrapper>().unwrap().deserialize();

            font_b.get_normal_variants().len().cmp(&font_a.get_normal_variants().len())
        };
        self.installed_model.insert_sorted(&object, move |a, b| fonts_compare(a, b));
    }

    pub fn set_sorting(&self, _sort_by: Option<SortBy>, _sort_order: Option<SortOrder>) {}
}
