use gio::prelude::*;

pub fn action<T, F>(thing: &T, name: &str, value: Option<&glib::VariantTy>, action: F) -> gio::SimpleAction
where
    T: glib::IsA<gio::ActionMap>,
    for<'r, 's> F: Fn(&'r gio::SimpleAction, Option<&glib::Variant>) + 'static,
{
    // Create a stateless, parameterless action
    let act = gio::SimpleAction::new(name, value);
    // Connect the handler
    act.connect_activate(action);
    // Add it to the map
    thing.add_action(&act);
    act
}

macro_rules! send {
    ($sender:expr, $action:expr) => {
        if let Err(err) = $sender.send($action) {
            log::error!("Failed to send \"{}\" action due to {}", stringify!($action), err);
        }
    };
}

macro_rules! spawn {
    ($future:expr) => {
        let ctx = glib::MainContext::default();
        ctx.spawn_local($future);
    };
}
