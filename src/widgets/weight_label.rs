use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

use crate::models::{FontCSSClass, FontWeight};

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct WeightLabel {
        pub label: gtk::Label,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for WeightLabel {
        const NAME: &'static str = "WeightLabel";
        type Type = super::WeightLabel;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            klass.set_layout_manager_type::<gtk::BinLayout>();
        }
    }

    impl ObjectImpl for WeightLabel {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            self.label.set_parent(obj);
            self.label.set_halign(gtk::Align::Start);
            self.label.add_css_class("weight-label");
        }

        fn dispose(&self, _obj: &Self::Type) {
            self.label.unparent();
        }
    }
    impl WidgetImpl for WeightLabel {}
}

glib::wrapper! {
    pub struct WeightLabel(ObjectSubclass<imp::WeightLabel>)
        @extends gtk::Widget;
}

impl WeightLabel {
    pub fn new(weight: FontWeight, font_family: &str) -> Self {
        let widget: Self = glib::Object::new(&[]).unwrap();
        let widget_ = imp::WeightLabel::from_instance(&widget);

        widget_.label.set_text(&weight.to_string());
        widget_.label.add_css_class(&weight.get_class());

        let attributes = pango::AttrList::new();
        attributes.change(pango::AttrString::new_family(font_family));
        widget_.label.set_attributes(Some(&attributes));

        widget
    }
}
