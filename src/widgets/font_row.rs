use crate::api::GoogleFont;
use crate::application::Action;
use crate::models::FontViewable;
use crate::widgets::VariantLabel;

use async_std::sync::Arc;
use gettextrs::ngettext;
use glib::Sender;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::CompositeTemplate;
use once_cell::sync::OnceCell;

pub enum RowAction {
    Installed,
    Downloaded,
    Removed,
    Loading,
}

mod imp {
    use super::*;

    #[derive(Default, CompositeTemplate)]
    #[template(resource = "/org/gnome/design/Lettercase/font_row.ui")]
    pub struct FontRow {
        pub sender: OnceCell<Sender<Action>>,
        pub font: OnceCell<Arc<dyn FontViewable>>,

        #[template_child]
        pub family_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub styles_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub variants_container: TemplateChild<gtk::Box>,
        #[template_child]
        pub status_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub click_gesture: TemplateChild<gtk::GestureClick>,
        #[template_child]
        pub install_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub loading_spinner: TemplateChild<gtk::Spinner>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for FontRow {
        const NAME: &'static str = "FontRow";
        type Type = super::FontRow;
        type ParentType = gtk::FlowBoxChild;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for FontRow {}
    impl WidgetImpl for FontRow {}
    impl FlowBoxChildImpl for FontRow {}
}

glib::wrapper! {
    pub struct FontRow(ObjectSubclass<imp::FontRow>)
        @extends gtk::Widget, gtk::FlowBoxChild;
}

impl FontRow {
    pub fn new(font: Arc<dyn FontViewable>, sender: Sender<Action>) -> Self {
        let row: Self = glib::Object::new(&[]).unwrap();
        let row_ = imp::FontRow::from_instance(&row);
        let _ = row_.sender.set(sender);
        let _ = row_.font.set(font);

        row.init();
        row
    }

    fn init(&self) {
        let self_ = imp::FontRow::from_instance(self);
        let font = self_.font.get().unwrap();
        let sender = self_.sender.get().unwrap();

        self_.family_label.set_text(&font.get_family());

        for variant in font.get_normal_variants() {
            let variant_label = VariantLabel::new(variant, &font.get_family());
            self_.variants_container.append(&variant_label);
        }

        if font.is_installed() {
            self_.status_stack.set_visible_child_name("installed");
        } else {
            self_.status_stack.set_visible_child_name("not_installed");
        }

        let len = font.get_variants().len() as u32;
        // TODO This won't work on gettext. See
        // https://savannah.gnu.org/bugs/?56774
        self_.styles_label.set_text(&ngettext!("{} style", "{} styles", len, len));

        self_.click_gesture.connect_released(glib::clone!(@strong font, @strong sender  => move |_, _, _, _| {
            send!(sender, Action::LoadFont(font.clone()));
        }));

        let (row_sender, receiver) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);

        if let Some(google_font) = font.as_any().downcast_ref::<GoogleFont>() {
            self_.install_button.connect_clicked(glib::clone!(@strong google_font, @strong row_sender, @strong sender => move |_| {
                send!(row_sender, RowAction::Loading);
                let row_sender = row_sender.clone();
                let sender = sender.clone();
                let google_font = google_font.clone();
                spawn!(async move {
                    google_font.download().await.and_then(|()| {
                        send!(sender, Action::FontInstalled);
                        send!(row_sender, RowAction::Installed);
                        Ok(())
                    });
                });
            }));
        }
        receiver.attach(None, glib::clone!(@weak self as obj => @default-return glib::Continue(false), move |action| obj.do_action(action)));

        let attributes = pango::AttrList::new();
        attributes.change(pango::AttrString::new_family(&font.get_family()));

        self_.family_label.set_attributes(Some(&attributes));
    }

    fn do_action(&self, action: RowAction) -> glib::Continue {
        let self_ = imp::FontRow::from_instance(self);

        match action {
            RowAction::Installed => {
                self_.loading_spinner.stop();
                self_.status_stack.set_visible_child_name("installed");
            }
            RowAction::Loading => {
                self_.loading_spinner.start();
                self_.status_stack.set_visible_child_name("loading");
            }
            _ => (),
        }
        glib::Continue(true)
    }
}
