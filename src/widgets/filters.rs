use gtk::glib;
use gtk::subclass::prelude::*;
use gtk::{prelude::*, CompositeTemplate};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/org/gnome/design/Lettercase/filters.ui")]
    pub struct FiltersWidget {
        #[template_child]
        pub child: TemplateChild<gtk::Widget>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for FiltersWidget {
        const NAME: &'static str = "FiltersWidget";
        type Type = super::FiltersWidget;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.set_layout_manager_type::<gtk::BinLayout>();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for FiltersWidget {
        fn dispose(&self, _obj: &Self::Type) {
            self.child.unparent();
        }
    }

    impl WidgetImpl for FiltersWidget {}
}

glib::wrapper! {
    pub struct FiltersWidget(ObjectSubclass<imp::FiltersWidget>)
        @extends gtk::Widget;
}

impl Default for FiltersWidget {
    fn default() -> Self {
        glib::Object::new(&[]).unwrap()
    }
}
