use std::cell::RefCell;

use crate::application::Action;
use crate::config;
use crate::config::{APP_ID, PROFILE};
use crate::models::FontViewable;
use crate::models::{SortBy, SortOrder};
use crate::utils;
use crate::widgets::{FiltersWidget, FontViewer, FontsLibrary};
use crate::window_state;

use async_std::sync::Arc;
use gio::prelude::*;
use glib::Sender;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::subclass::widget::WidgetImplExt;
use gtk::CompositeTemplate;
use libadwaita::subclass::prelude::*;

use gettextrs::gettext;
use once_cell::sync::OnceCell;

#[derive(PartialEq, Debug)]
pub enum View {
    Loading,
    Library,
    Font,
}

#[derive(Debug, CompositeTemplate)]
#[template(resource = "/org/gnome/design/Lettercase/window.ui")]
pub struct WindowPriv {
    sender: OnceCell<Sender<Action>>,
    settings: gio::Settings,
    library: OnceCell<FontsLibrary>,
    viewer: OnceCell<FontViewer>,
    progress_bar_pulse: RefCell<Option<glib::SourceId>>,

    #[template_child]
    pub progress_bar: TemplateChild<gtk::ProgressBar>,
    #[template_child]
    pub status_page: TemplateChild<libadwaita::StatusPage>,
    #[template_child]
    pub filters_stack: TemplateChild<gtk::Stack>,
    #[template_child]
    pub library_stack: TemplateChild<libadwaita::ViewStack>,
    #[template_child]
    pub search_stack: TemplateChild<gtk::Stack>,
    #[template_child]
    pub main_stack: TemplateChild<gtk::Stack>,
    #[template_child]
    pub header_stack: TemplateChild<gtk::Stack>,
    #[template_child]
    pub viewer_container: TemplateChild<gtk::Box>,
    #[template_child]
    pub installed_container: TemplateChild<gtk::Box>,
    #[template_child]
    pub window_content: TemplateChild<gtk::Box>,
    #[template_child]
    pub explore_container: TemplateChild<gtk::Box>,
    #[template_child]
    pub search_entry: TemplateChild<gtk::SearchEntry>,
    #[template_child]
    pub filters: TemplateChild<FiltersWidget>,
}

#[glib::object_subclass]
impl ObjectSubclass for WindowPriv {
    const NAME: &'static str = "LcApplicationWindow";
    type ParentType = libadwaita::ApplicationWindow;
    type Type = Window;

    fn new() -> Self {
        Self {
            sender: OnceCell::new(),
            settings: gio::Settings::new(APP_ID),
            library: OnceCell::new(),
            viewer: OnceCell::new(),
            progress_bar_pulse: RefCell::new(None),
            status_page: TemplateChild::default(),
            progress_bar: TemplateChild::default(),
            library_stack: TemplateChild::default(),
            filters_stack: TemplateChild::default(),
            search_stack: TemplateChild::default(),
            main_stack: TemplateChild::default(),
            header_stack: TemplateChild::default(),
            viewer_container: TemplateChild::default(),
            installed_container: TemplateChild::default(),
            window_content: TemplateChild::default(),
            explore_container: TemplateChild::default(),
            search_entry: TemplateChild::default(),
            filters: TemplateChild::default(),
        }
    }

    fn class_init(klass: &mut Self::Class) {
        Self::bind_template(klass);
    }

    fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
        obj.init_template();
    }
}

impl ObjectImpl for WindowPriv {}

impl WidgetImpl for WindowPriv {
    fn size_allocate(&self, widget: &Self::Type, width: i32, height: i32, baseline: i32) {
        if width <= 700 {
            self.filters_stack.set_visible_child_name("mobile");
            self.search_stack.set_visible_child_name("button");
        } else {
            self.filters_stack.set_visible_child_name("large");
            self.search_stack.set_visible_child_name("entry");
        }

        self.parent_size_allocate(widget, width, height, baseline);
    }
}

impl WindowImpl for WindowPriv {
    fn close_request(&self, window: &Self::Type) -> glib::signal::Inhibit {
        if let Err(err) = window_state::save(window, &self.settings) {
            log::warn!("Failed to save window state {}", err);
        }

        self.parent_close_request(window)
    }
}

impl ApplicationWindowImpl for WindowPriv {}
impl AdwApplicationWindowImpl for WindowPriv {}

glib::wrapper! {
    pub struct Window(ObjectSubclass<WindowPriv>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, libadwaita::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl Window {
    pub fn new(app: &crate::application::Application, sender: Sender<Action>) -> Self {
        let window: Window = glib::Object::new(&[("application", app), ("icon-name", &APP_ID.to_owned())]).unwrap();
        let self_ = WindowPriv::from_instance(&window);

        let library = FontsLibrary::new(sender.clone());
        let viewer = FontViewer::new();

        self_.sender.set(sender).unwrap();
        self_.library.set(library).unwrap();
        self_.viewer.set(viewer).unwrap();

        window.init();
        window.setup_actions();
        window.set_view(View::Loading);

        let progress = self_.progress_bar.get();
        let source = glib::timeout_add_local(
            std::time::Duration::from_millis(100),
            glib::clone!(@weak progress => @default-return Continue(false), move || {
                progress.pulse();

                Continue(true)
            }),
        );
        self_.progress_bar_pulse.replace(Some(source));

        window
    }

    pub fn load_font(&self, font: Arc<dyn FontViewable>) {
        let self_ = WindowPriv::from_instance(self);
        let viewer = self_.viewer.get().unwrap();
        viewer.load(font);
        self.set_view(View::Font);
    }

    pub fn set_view(&self, view: View) {
        let self_ = WindowPriv::from_instance(self);
        match view {
            View::Loading => {
                self_.main_stack.set_visible_child_name("loading");
                self_.header_stack.set_visible_child_name("loading");
            }
            View::Library => {
                self_.main_stack.set_visible_child_name("library");
                self_.header_stack.set_visible_child_name("library");

                if let Some(id) = self_.progress_bar_pulse.borrow_mut().take() {
                    id.remove();
                }
            }
            View::Font => {
                self_.main_stack.set_visible_child_name("viewer");
                self_.header_stack.set_visible_child_name("viewer");
            }
        }
    }

    fn init(&self) {
        let self_ = WindowPriv::from_instance(self);

        if PROFILE == "Devel" {
            self.style_context().add_class("devel");
            self_.status_page.set_icon_name(Some("org.gnome.design.LettercaseDevel"))
        }

        let library = self_.library.get().unwrap();
        let viewer = self_.viewer.get().unwrap();

        // load latest window state
        let settings = &self_.settings;
        window_state::load(self, settings);

        // The font library
        self_.explore_container.append(&library.explore_library);
        self_.installed_container.append(&library.installed_library);
        self_.viewer_container.append(viewer);

        // Bind the on change event for search entry
        self_.search_entry.connect_search_changed(glib::clone!(@weak self as window => move |_| {
            window.search();
        }));

        // When we swtich view, we search for what was already in the search bar.
        self_.library_stack.connect_visible_child_notify(glib::clone!(@weak self as window => move |_| {
            window.search();
        }));
    }

    fn search(&self) {
        let self_ = WindowPriv::from_instance(self);
        let searched_text = self_.search_entry.text();

        let lib = self_.library.get().unwrap();
        if self_.library_stack.visible_child_name().as_deref() == Some("explore") {
            lib.explore_library.search(&searched_text);
        } else {
            lib.installed_library.search(&searched_text);
        }
    }

    fn setup_actions(&self) {
        let self_ = WindowPriv::from_instance(self);
        let actions = gio::SimpleActionGroup::new();
        let sort_descending_action = gio::SimpleAction::new("sort-descending", None);
        let sort_ascending_action = gio::SimpleAction::new("sort-ascending", None);

        sort_descending_action.connect_activate(glib::clone!(@strong sort_ascending_action, @weak self as window => move |action, _| {
            let s = WindowPriv::from_instance(&window);
            let lib = s.library.get().unwrap();
            action.set_enabled(false);
            sort_ascending_action.set_enabled(true);
            lib.sort(None, Some(SortOrder::Desc));
        }));
        actions.add_action(&sort_descending_action);

        sort_ascending_action.connect_activate(glib::clone!(@strong sort_descending_action, @weak self as window => move |action, _| {
            let s = WindowPriv::from_instance(&window);
            let lib = s.library.get().unwrap();
            action.set_enabled(false);
            sort_descending_action.set_enabled(true);
            lib.sort(None, Some(SortOrder::Asc));
        }));
        sort_ascending_action.set_enabled(false); // We start with Asc order.
        actions.add_action(&sort_ascending_action);

        // About
        let simple_action = gio::SimpleAction::new("about", None);
        simple_action.connect_activate(glib::clone!(@weak self as window => move |_, _| {
            let about_dialog = gtk::AboutDialog::builder()
                .authors(vec![
                    "Bilal Elmoussaoui".to_string(),
                    "Christopher Davis".to_string(),
                    "Maximiliano Sandoval".to_string(),
                ])
                .artists(vec!["Tobias Bernard".to_string()])
                .license_type(gtk::License::Gpl30)
                .version(config::VERSION)
                .translator_credits(&gettext("translator-credits"))
                .logo_icon_name(config::APP_ID)
                .website("https://gitlab.gnome.org/World/design/Lettercase/")
                .modal(true)
                .transient_for(&window)
                .build();

            about_dialog.show();
        }));
        actions.add_action(&simple_action);

        utils::action(
            &actions,
            "sort-by",
            Some(glib::VariantTy::new("s").unwrap()),
            glib::clone!(@weak self as window => move |_, data| {
                let s = WindowPriv::from_instance(&window);
                let lib = s.library.get().unwrap();
                let query: String = data.unwrap().get::<String>().unwrap();
                let sort_by = SortBy::from(query.as_str());
                lib.sort(Some(sort_by), None);
            }),
        );

        utils::action(
            &actions,
            "open",
            None,
            glib::clone!(@strong self_.sender as sender_cell => move |_, _| {
                let sender = sender_cell.get().unwrap();
                send!(sender, Action::ViewLibrary);
            }),
        );
        self.insert_action_group("library", Some(&actions));

        self.add_action(&simple_action);
    }

    pub fn refresh_library(&self) {
        let self_ = WindowPriv::from_instance(self);
        let library = self_.library.get().unwrap();
        library.refresh_installed();
    }
}
