use crate::models::{FontStyle, FontViewable};
use crate::widgets::VariantLabel;

use async_std::sync::Arc;

use gtk::glib;
use gtk::subclass::prelude::*;
use gtk::{prelude::*, CompositeTemplate};
use libadwaita::prelude::*;

const CHARACTERS: [&'static str; 93] = [
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
    "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", r#"‘"#, r#"?"#, r#"’"#, r#"“"#, r#"!"#, r#"”"#, r#"("#, r#"%"#, r#")"#, r#"["#,
    r#"#"#, r#"]"#, r#"{"#, r#"@"#, r#"}"#, r#"/"#, r#"&"#, r#"\"#, r#"<"#, r#"-"#, r#"+"#, r#","#, r#">"#, r#","#, r#","#, r#","#, r#":"#, r#";"#, r#","#, r#"."#, r#"*"#,
];

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/org/gnome/design/Lettercase/font_viewer.ui")]
    pub struct FontViewer {
        #[template_child]
        pub child: TemplateChild<gtk::Widget>,
        #[template_child]
        pub family_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub demo_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub variants_container: TemplateChild<libadwaita::Bin>,
        #[template_child]
        pub characters_container: TemplateChild<libadwaita::Bin>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for FontViewer {
        const NAME: &'static str = "FontViewer";
        type Type = super::FontViewer;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.set_layout_manager_type::<gtk::BinLayout>();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for FontViewer {
        fn dispose(&self, _obj: &Self::Type) {
            self.child.unparent();
        }
    }
    impl WidgetImpl for FontViewer {}
}

glib::wrapper! {
    pub struct FontViewer(ObjectSubclass<imp::FontViewer>)
        @extends gtk::Widget;
}

impl FontViewer {
    pub fn new() -> Self {
        glib::Object::new(&[]).unwrap()
    }

    pub fn load(&self, font: Arc<dyn FontViewable>) {
        let self_ = imp::FontViewer::from_instance(self);
        self_.family_label.set_text(&font.get_family());

        let attributes = pango::AttrList::new();
        attributes.change(pango::AttrString::new_family(&font.get_family()));

        self_.family_label.set_attributes(Some(&attributes));
        self_.demo_label.set_attributes(Some(&attributes));

        let grid = gtk::Grid::new();
        grid.set_halign(gtk::Align::Start);
        grid.set_column_spacing(24);
        grid.set_row_spacing(6);

        for (row, var) in font.get_normal_variants().iter().enumerate() {
            let weight = var.weight;
            for variant in font.get_variants() {
                if variant.weight == weight {
                    let column = match variant.style {
                        FontStyle::Italic => 1,
                        FontStyle::Oblique => 2,
                        FontStyle::Normal => 0,
                    };
                    let variant_label = VariantLabel::new(variant, &font.get_family());
                    grid.attach(&variant_label, column, row as i32, 1, 1);
                }
            }
        }

        self_.variants_container.set_child(Some(&grid));

        let flowbox = gtk::FlowBox::new();
        flowbox.set_selection_mode(gtk::SelectionMode::None);
        flowbox.set_can_focus(false);
        flowbox.set_max_children_per_line(30);
        flowbox.set_column_spacing(6);
        flowbox.set_row_spacing(6);

        // TODO This always shows the same characters, it should
        // be a sample based on whats shipped with the font.
        for character in CHARACTERS.iter() {
            let label = gtk::Label::new(Some(character));
            // FIXME Requires the v4_6 feature.
            // flowbox.append(&label);
            label.set_attributes(Some(&attributes));
            flowbox.insert(&label, -1);
        }

        self_.characters_container.set_child(Some(&flowbox));
    }
}
