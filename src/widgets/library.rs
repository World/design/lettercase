use crate::api::{GoogleFont, SystemFont};
use crate::application::Action;
use crate::models::{FontsModel, ObjectWrapper, SortBy, SortOrder};
use crate::widgets::FontRow;

use async_std::sync::Arc;
use glib::Sender;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::CompositeTemplate;
use std::cell::RefCell;

#[derive(Debug)]
pub struct FontsLibrary {
    model: RefCell<Arc<FontsModel>>,
    sender: Sender<Action>,
    pub installed_library: LcLibraryList,
    pub explore_library: LcLibraryList,
}

impl FontsLibrary {
    pub fn new(sender: Sender<Action>) -> Self {
        let model = RefCell::new(FontsModel::new(sender.clone()));

        let installed_library = LcLibraryList::new();
        let explore_library = LcLibraryList::new();

        let library = Self {
            installed_library,
            explore_library,
            model,
            sender,
        };
        library.init();
        library
    }

    pub fn refresh_installed(&self) {
        let model = self.model.borrow().clone();
        spawn!(async move {
            model.refresh_system_fonts().await;
        });
    }

    pub fn sort(&self, sort_by: Option<SortBy>, sort_order: Option<SortOrder>) {
        self.model.borrow().set_sorting(sort_by, sort_order);
    }

    fn init(&self) {
        self.installed_library.bind_model(
            &self.model.borrow().installed_model,
            glib::clone!(@strong self.sender as sender => move |font| {
                let font: SystemFont = font.downcast_ref::<ObjectWrapper>().unwrap().deserialize();
                let row = FontRow::new(Arc::new(font), sender.clone());
                row.upcast::<gtk::Widget>()
            }),
        );

        self.explore_library.bind_model(
            &self.model.borrow().explore_model,
            glib::clone!(@strong self.sender as sender => move |font| {
                let font: GoogleFont = font.downcast_ref::<ObjectWrapper>().unwrap().deserialize();
                let row = FontRow::new(Arc::new(font), sender.clone());
                row.upcast::<gtk::Widget>()
            }),
        );
    }
}

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/org/gnome/design/Lettercase/fonts_library.ui")]
    pub struct LcLibraryList {
        pub filter: RefCell<Option<gtk::FilterListModel>>,
        #[template_child]
        pub flowbox: TemplateChild<gtk::FlowBox>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LcLibraryList {
        const NAME: &'static str = "LcLibraryList";
        type ParentType = gtk::Widget;
        type Type = super::LcLibraryList;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for LcLibraryList {
        fn dispose(&self, obj: &Self::Type) {
            if let Some(child) = obj.first_child() {
                while let Some(sibling) = child.next_sibling() {
                    sibling.unparent();
                }
                child.unparent();
            }
        }
    }

    impl WidgetImpl for LcLibraryList {}
}

glib::wrapper! {
    pub struct LcLibraryList(ObjectSubclass<imp::LcLibraryList>) @extends gtk::Widget;
}

impl LcLibraryList {
    pub fn new() -> Self {
        glib::Object::new(&[]).unwrap()
    }

    pub fn search(&self, text: &str) {
        let imp = imp::LcLibraryList::from_instance(self);

        let text = text.to_string();
        if text.is_empty() {
            imp.filter.borrow().as_ref().unwrap().set_filter(gtk::Filter::NONE);
        } else {
            let filter = gtk::CustomFilter::new(move |obj| {
                let font = obj.downcast_ref::<ObjectWrapper>().unwrap();

                font.family_name().to_lowercase().contains(&text.to_lowercase())
            });
            imp.filter.borrow().as_ref().unwrap().set_filter(Some(&filter));
        }
    }

    pub fn bind_model<F>(&self, model: &gio::ListStore, callback: F)
    where
        for<'r> F: std::ops::Fn(&'r glib::Object) -> gtk::Widget + 'static,
    {
        let imp = imp::LcLibraryList::from_instance(self);
        let filter = gtk::FilterListModel::new(Some(model), gtk::Filter::NONE);
        imp.flowbox.bind_model(Some(&filter), callback);
        imp.filter.replace(Some(filter));
    }
}
