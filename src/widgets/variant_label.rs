use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

use crate::models::{FontCSSClass, FontVariant};

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct VariantLabel {
        pub label: gtk::Label,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for VariantLabel {
        const NAME: &'static str = "VariantLabel";
        type Type = super::VariantLabel;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            klass.set_layout_manager_type::<gtk::BinLayout>();
        }
    }

    impl ObjectImpl for VariantLabel {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            self.label.set_parent(obj);
            self.label.add_css_class("variant-label");
            self.label.set_halign(gtk::Align::Start);
        }

        fn dispose(&self, _obj: &Self::Type) {
            self.label.unparent();
        }
    }
    impl WidgetImpl for VariantLabel {}
}

glib::wrapper! {
    pub struct VariantLabel(ObjectSubclass<imp::VariantLabel>)
        @extends gtk::Widget;
}

impl VariantLabel {
    pub fn new(variant: FontVariant, font_family: &str) -> Self {
        let widget: Self = glib::Object::new(&[]).unwrap();
        let widget_ = imp::VariantLabel::from_instance(&widget);

        widget_.label.set_text(&variant.to_string());
        widget_.label.add_css_class(&variant.style.get_class());
        widget_.label.add_css_class(&variant.weight.get_class());

        let attributes = pango::AttrList::new();
        attributes.change(pango::AttrString::new_family(font_family));
        widget_.label.set_attributes(Some(&attributes));

        widget
    }
}
