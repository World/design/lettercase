use crate::config;
use crate::models::FontViewable;
use crate::widgets::{View, Window};

use async_std::sync::Arc;
use gio::prelude::*;
use glib::subclass::prelude::*;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use libadwaita::subclass::prelude::*;

pub enum Action {
    ViewLibrary,
    LoadFont(Arc<dyn FontViewable>),
    FontInstalled, // Refresh the cache of system fonts
}
unsafe impl Send for Action {}

#[derive(Default)]
pub struct ApplicationPriv {}

#[glib::object_subclass]
impl ObjectSubclass for ApplicationPriv {
    const NAME: &'static str = "LcApplication";
    type ParentType = libadwaita::Application;
    type Type = Application;
}

impl ObjectImpl for ApplicationPriv {}

impl ApplicationImpl for ApplicationPriv {
    fn activate(&self, app: &Self::Type) {
        self.parent_activate(app);

        if let Some(window) = app.active_window() {
            window.present();
            return;
        }

        let (sender, r) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let window = Window::new(&app, sender);
        window.present();

        r.attach(None, glib::clone!(@weak app => @default-return glib::Continue(false), move |action| app.do_action(action)));
    }

    fn startup(&self, application: &Self::Type) {
        self.parent_startup(application);

        log::info!("{}Lettercase ({})", config::NAME_PREFIX, config::APP_ID);
        log::info!("Version: {} ({})", config::VERSION, config::PROFILE);
        log::info!("Datadir: {}", config::PKGDATADIR);

        application.setup_gactions();
    }
}

impl GtkApplicationImpl for ApplicationPriv {}
impl AdwApplicationImpl for ApplicationPriv {}

glib::wrapper! {
    pub struct Application(ObjectSubclass<ApplicationPriv>)
        @extends gio::Application, gtk::Application, libadwaita::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl Application {
    pub fn new() -> Self {
        glib::Object::new(&[
            ("application-id", &config::APP_ID.to_owned()),
            ("flags", &gio::ApplicationFlags::default()),
            ("resource-base-path", &Some("/org/gnome/design/Lettercase/")),
        ])
        .unwrap()
    }

    fn setup_gactions(&self) {
        // Quit
        let simple_action = gio::SimpleAction::new("quit", None);
        simple_action.connect_activate(glib::clone!(@weak self as app => move |_, _| app.quit()));
        self.add_action(&simple_action);
        self.set_accels_for_action("app.quit", &["<Control>q"]);
    }

    fn do_action(&self, action: Action) -> glib::Continue {
        let window = self.active_window().unwrap().downcast::<Window>().unwrap();
        match action {
            Action::ViewLibrary => window.set_view(View::Library),
            Action::LoadFont(font) => window.load_font(font),
            Action::FontInstalled => window.refresh_library(),
        };
        glib::Continue(true)
    }
}
