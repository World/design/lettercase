use super::system;
use super::utils;
use crate::models::{FontStyle, FontVariant, FontViewable, FontWeight};

use async_std::fs;
use futures_util::AsyncWriteExt;
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use std::any::Any;
use std::path::PathBuf;
use url::Url;

static API_KEY: Lazy<String> = Lazy::new(|| "AIzaSyDVz9TPd0lU7Cbr8jMqkzgL1SoRkcjx1-E".to_string());
static BASE_URI: Lazy<String> = Lazy::new(|| "https://www.googleapis.com/webfonts/v1/webfonts".to_string());
static CACHE_DIR: Lazy<PathBuf> = Lazy::new(|| [glib::user_cache_dir().to_str().unwrap(), "fonts"].iter().collect());

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
struct Root {
    kind: String,
    items: Vec<GoogleFont>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct GoogleFont {
    kind: String,
    family: String,
    category: String,
    variants: Vec<String>,
    subsets: Vec<String>,
    version: String,
    #[serde(rename = "lastModified")]
    last_modified: String,
    files: Files,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
struct Files {
    #[serde(rename = "100")]
    n100: Option<String>,
    #[serde(rename = "100italic")]
    n100_italic: Option<String>,
    #[serde(rename = "300")]
    n300: Option<String>,
    #[serde(rename = "300italic")]
    n300_italic: Option<String>,
    regular: Option<String>,
    italic: Option<String>,
    #[serde(rename = "500")]
    n500: Option<String>,
    #[serde(rename = "500italic")]
    n500_italic: Option<String>,
    #[serde(rename = "700")]
    n700: Option<String>,
    #[serde(rename = "700italic")]
    n700_italic: Option<String>,
    #[serde(rename = "900")]
    n900: Option<String>,
    #[serde(rename = "900italic")]
    n900_italic: Option<String>,
    #[serde(rename = "600")]
    n600: Option<String>,
    #[serde(rename = "600italic")]
    n600_italic: Option<String>,
    #[serde(rename = "800")]
    n800: Option<String>,
    #[serde(rename = "800italic")]
    n800_italic: Option<String>,
    #[serde(rename = "200")]
    n200: Option<String>,
    #[serde(rename = "200italic")]
    n200_italic: Option<String>,
}

fn get_font_cache(font_uri: &Url) -> PathBuf {
    let filename = font_uri.path_segments().map(|c| c.collect::<Vec<_>>()).unwrap().pop().unwrap();

    let mut cache_file: PathBuf = CACHE_DIR.clone();
    std::fs::create_dir_all(cache_file.to_str().unwrap()).unwrap();
    cache_file.push(filename);

    cache_file
}

impl GoogleFont {
    fn get_variant_uri(&self, variant: &str) -> Option<String> {
        match variant {
            "regular" => self.files.regular.clone(),
            "italic" => self.files.italic.clone(),
            "100" => self.files.n100.clone(),
            "100italic" => self.files.n100_italic.clone(),
            "200" => self.files.n200.clone(),
            "200italic" => self.files.n200_italic.clone(),
            "300" => self.files.n300.clone(),
            "300italic" => self.files.n300_italic.clone(),
            "500" => self.files.n500.clone(),
            "500italic" => self.files.n500_italic.clone(),
            "600" => self.files.n600.clone(),
            "600italic" => self.files.n600_italic.clone(),
            "700" => self.files.n700.clone(),
            "700italic" => self.files.n700_italic.clone(),
            "800" => self.files.n800.clone(),
            "800italic" => self.files.n800_italic.clone(),
            "900" => self.files.n900.clone(),
            "900italic" => self.files.n900_italic.clone(),
            _ => {
                log::error!("Failed to find font variant: {}", variant);
                None
            }
        }
    }

    fn get_font_uris(&self) -> Vec<Url> {
        let mut uris = vec![];
        for variant in self.variants.iter() {
            if let Some(font_uri) = self.get_variant_uri(variant) {
                let uri = Url::parse(&font_uri).unwrap(); // we are sure the url is valid
                uris.push(uri);
            }
        }
        uris
    }

    pub async fn download(&self) -> Result<(), Box<dyn std::error::Error + std::marker::Send + std::marker::Sync>> {
        for font_uri in self.get_font_uris().iter() {
            let cache_file = get_font_cache(&font_uri);
            if !cache_file.exists() {
                log::info!("downloading file {:#?}", font_uri);
                let bytes = surf::get(font_uri).await?.body_bytes().await?;
                let mut file = fs::File::create(cache_file).await?;
                file.write(&bytes).await?;
            }
        }
        self.install().await?;
        Ok(())
    }

    pub async fn install(&self) -> Result<(), Box<dyn std::error::Error + std::marker::Send + std::marker::Sync>> {
        let font_dir = system::CACHE_DIR.join(&self.family);
        fs::create_dir_all(&font_dir).await?;
        for font_uri in self.get_font_uris().iter() {
            let cache_file = get_font_cache(&font_uri);
            let dest = font_dir.join(cache_file.file_name().unwrap());
            fs::copy(cache_file, dest).await?;
        }
        utils::refresh_cache().await;
        Ok(())
    }
}

impl FontViewable for GoogleFont {
    fn get_variants(&self) -> Vec<FontVariant> {
        let mut variants = Vec::new();
        for variant in self.variants.iter() {
            let font_variant = match variant.as_str() {
                "italic" => FontVariant {
                    style: FontStyle::Italic,
                    weight: FontWeight::Normal,
                },
                "100" => FontVariant {
                    style: FontStyle::Normal,
                    weight: FontWeight::Thin,
                },
                "100italic" => FontVariant {
                    style: FontStyle::Italic,
                    weight: FontWeight::Thin,
                },
                "200" => FontVariant {
                    style: FontStyle::Normal,
                    weight: FontWeight::ExtraLight,
                },
                "200italic" => FontVariant {
                    style: FontStyle::Italic,
                    weight: FontWeight::ExtraLight,
                },
                "300" => FontVariant {
                    style: FontStyle::Normal,
                    weight: FontWeight::Light,
                },
                "300italic" => FontVariant {
                    style: FontStyle::Italic,
                    weight: FontWeight::Light,
                },
                "500" => FontVariant {
                    style: FontStyle::Normal,
                    weight: FontWeight::Medium,
                },
                "500italic" => FontVariant {
                    style: FontStyle::Italic,
                    weight: FontWeight::Medium,
                },
                "600" => FontVariant {
                    style: FontStyle::Normal,
                    weight: FontWeight::SemiBold,
                },
                "600italic" => FontVariant {
                    style: FontStyle::Italic,
                    weight: FontWeight::SemiBold,
                },
                "700" => FontVariant {
                    style: FontStyle::Normal,
                    weight: FontWeight::Bold,
                },
                "700italic" => FontVariant {
                    style: FontStyle::Italic,
                    weight: FontWeight::Bold,
                },
                "800" => FontVariant {
                    style: FontStyle::Normal,
                    weight: FontWeight::ExtraBold,
                },
                "800italic" => FontVariant {
                    style: FontStyle::Italic,
                    weight: FontWeight::ExtraBold,
                },
                "900" => FontVariant {
                    style: FontStyle::Normal,
                    weight: FontWeight::Heavy,
                },
                "900italic" => FontVariant {
                    style: FontStyle::Italic,
                    weight: FontWeight::Heavy,
                },
                _ => FontVariant {
                    style: FontStyle::Normal,
                    weight: FontWeight::Normal,
                },
            };
            variants.push(font_variant);
        }
        variants
    }

    fn is_installed(&self) -> bool {
        for font_uri in self.get_font_uris() {
            let cache_file = get_font_cache(&font_uri);
            if !cache_file.exists() {
                return false;
            }
        }
        true
    }

    fn get_family(&self) -> String {
        self.family.clone()
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
}

pub async fn fetch_fonts() -> Result<Vec<GoogleFont>, Box<dyn std::error::Error + std::marker::Send + std::marker::Sync>> {
    let mut api_uri = Url::parse(&BASE_URI)?; // we are sure the url is valid
    api_uri.set_query(Some(&format!("key={}", API_KEY.as_str())));

    let result: Root = surf::get(api_uri).recv_json().await?;
    Ok(result.items)
}
