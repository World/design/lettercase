use crate::models::{FontStyle, FontVariant, FontViewable, FontWeight};
use font_kit::sources::{fontconfig::FontconfigSource, fs::FsSource, multi::MultiSource};
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use std::any::Any;
use std::collections::HashMap;
use std::path::PathBuf;

pub static CACHE_DIR: Lazy<PathBuf> = Lazy::new(|| [glib::home_dir().to_str().unwrap(), ".local", "share", "fonts"].iter().collect());

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct SystemFont {
    family: String,
    variants: Vec<FontVariant>,
}

impl SystemFont {
    pub fn add_variant(&mut self, variant: FontVariant) {
        match self.variants.binary_search(&variant) {
            Err(i) => self.variants.insert(i, variant),
            Ok(_) => return,
        }
    }
}

impl FontViewable for SystemFont {
    fn get_variants(&self) -> Vec<FontVariant> {
        self.variants.clone()
    }

    fn is_installed(&self) -> bool {
        true
    }

    fn get_family(&self) -> String {
        self.family.clone()
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
}

impl From<font_kit::properties::Style> for FontStyle {
    fn from(style: font_kit::properties::Style) -> Self {
        match style {
            font_kit::properties::Style::Normal => FontStyle::Normal,
            font_kit::properties::Style::Italic => FontStyle::Italic,
            font_kit::properties::Style::Oblique => FontStyle::Oblique,
        }
    }
}
impl From<font_kit::properties::Weight> for FontWeight {
    fn from(weight: font_kit::properties::Weight) -> Self {
        match weight.0 as usize {
            100 => FontWeight::Thin,
            200 => FontWeight::ExtraLight,
            300 => FontWeight::Light,
            500 => FontWeight::Medium,
            600 => FontWeight::SemiBold,
            700 => FontWeight::Bold,
            800 => FontWeight::ExtraBold,
            900 => FontWeight::Heavy,
            _ => FontWeight::Normal,
        }
    }
}

pub fn fetch_fonts(tx: async_std::channel::Sender<Vec<SystemFont>>) {
    let mut sources: Vec<Box<dyn font_kit::source::Source>> = Vec::new();
    sources.push(Box::new(FsSource::new()));
    sources.push(Box::new(FontconfigSource::new()));

    let source = MultiSource::from_sources(sources);

    let source_fonts = source.all_fonts().unwrap();
    async_std::task::spawn(async move {
        let mut fonts: HashMap<String, SystemFont> = HashMap::new();
        for handle in source_fonts.iter() {
            if let Ok(font) = handle.load() {
                let family_name = font.family_name();
                match fonts.get_mut(&family_name) {
                    Some(system_font) => {
                        system_font.add_variant(FontVariant {
                            style: FontStyle::from(font.properties().style),
                            weight: FontWeight::from(font.properties().weight),
                        });
                    }
                    None => {
                        fonts.insert(
                            family_name,
                            SystemFont {
                                family: font.family_name(),
                                variants: Vec::new(),
                            },
                        );
                    }
                }
            }
        }
        let fonts = fonts.iter().map(|(_, font)| font.clone()).collect::<Vec<SystemFont>>();
        tx.send(fonts).await.unwrap();
    });
}
