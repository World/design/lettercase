use std::process::Command;

pub async fn refresh_cache() {
    async_std::task::spawn(async move {
        log::info!("updating font cache");
        let fonts_dir = glib::home_dir().join(".local").join("share").join("fonts");
        if let Err(err) = Command::new("fc-cache").arg("-f").arg("-v").arg(fonts_dir).output() {
            log::warn!("Failed to refresh font config cache {}", err)
        }
    })
    .await;
}
