use gio::prelude::SettingsExt;
use gtk::prelude::GtkWindowExt;

pub fn load(window: &crate::widgets::Window, settings: &gio::Settings) {
    let width = settings.int("window-width");
    let height = settings.int("window-height");

    if width > -1 && height > -1 {
        window.set_default_size(width, height);
    }

    let is_maximized = settings.boolean("is-maximized");

    if is_maximized {
        window.maximize();
    }
}

pub fn save(window: &crate::widgets::Window, settings: &gio::Settings) -> Result<(), glib::BoolError> {
    let size = window.default_size();

    settings.set_int("window-width", size.0)?;
    settings.set_int("window-height", size.1)?;

    settings.set_boolean("is-maximized", window.is_maximized())?;

    Ok(())
}
