use gettextrs::*;
#[macro_use]
mod utils;

mod api;
mod application;
mod config;
mod models;

mod widgets;
mod window_state;

use application::Application;
use config::{LOCALEDIR, PACKAGE_NAME};
use gio::prelude::ApplicationExtManual;

fn main() {
    pretty_env_logger::init();

    glib::set_application_name(&format!("{}Lettercase", config::NAME_PREFIX));

    // Prepare i18n
    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain(PACKAGE_NAME, LOCALEDIR).unwrap();
    textdomain(PACKAGE_NAME).unwrap();

    let res = gio::Resource::load(config::PKGDATADIR.to_owned() + "/" + &config::APP_ID.to_owned() + ".gresource").expect("Could not load resources");
    gio::resources_register(&res);

    let app = Application::new();
    app.run();
}
